<?php

namespace Presentdv\AddressSuggestions\Service;


use Presentdv\AddressSuggestions\GeoObject\GeoObjectInterface;

interface AddressSuggestionsServiceInterface
{
    /**
     * @param string $address
     * @param int $limit
     * @return GeoObjectInterface[]
     */
    public function findByKhabarovsk(string $address, int $limit): array;

    /**
     * @param string $address
     * @param int $limit
     * @return GeoObjectInterface[]
     */
    public function findByKhabarovskRegion(string $address, int $limit): array;

    /**
     * @param string $address
     * @param int $limit
     * @return GeoObjectInterface[]
     */
    public function findByRussia(string $address, int $limit): array;
}