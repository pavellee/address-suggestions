<?php

namespace Presentdv\AddressSuggestions\Service;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Presentdv\AddressSuggestions\GeoObject\GeoObject;
use function GuzzleHttp\Psr7\build_query;

class NominatimSuggestionsService extends AddressSuggestionsService
{
    public function findByKhabarovsk(string $address, int $limit): array
    {
        $query_array = [
            'Хабаровский край',
            'город Хабаровск',
            $address
        ];

        $query = implode(', ', $query_array);

        $prompts = $this->getPrompts($query, $limit);

        return $this->filterKhabarovsk($prompts);
    }

    public function findByKhabarovskRegion(string $address, int $limit): array
    {
        $query_array = [
            'Хабаровский край',
            $address
        ];

        $query = implode(', ', $query_array);

        $prompts = $this->getPrompts($query, $limit);

        return $this->filterKhabarovskRegion($prompts);
    }

    public function findByRussia(string $address, int $limit): array
    {
        return $this->getPrompts($address, $limit);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Presentdv\AddressSuggestions\AddressSuggestionsException
     */
    private function getPrompts(string $query, int $limit): array
    {
        $body = [
            'addressdetails' => 1,
            'format' => 'json',
            'limit' => $limit,
            'q' => $query
        ];

        $client = new Client([
            'timeout' => self::CLIENT_TIMEOUT
        ]);

        $request_query = build_query($body);

        try {

            $response = $client->request('GET', $this->url . '?' . $request_query);

            $contents = $response->getBody()->getContents();

            $results = \GuzzleHttp\json_decode($contents, true);

        } catch (RequestException $exception) {
            $this->handleRequestException($exception);
        }

        return array_map(function ($data) {
            return GeoObject::fromNominatimData($data);
        }, $results);
    }

    private function filterKhabarovsk(array $prompts): array
    {
        foreach ($prompts as $index => $prompt) {

            if (!isset($prompt['address']['state']) ||
                $prompt['address']['state'] !== 'Хабаровский край' ||
                !isset($prompt['address']['city']) ||
                $prompt['address']['city'] !== 'Хабаровск'
            ) {
                unset($prompts[$index]);
            }
        }

        return $prompts;
    }

    private function filterKhabarovskRegion(array $prompts): array
    {
        foreach ($prompts as $index => $prompt) {

            if (!isset($prompt['address']['state']) || $prompt['address']['state'] !== 'Хабаровский край') {
                unset($prompt[$index]);
            }
        }

        return $prompts;
    }
}