<?php

namespace Presentdv\AddressSuggestions\Service;


use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Presentdv\AddressSuggestions\AddressSuggestionsException;
use Presentdv\AddressSuggestions\GeoObject\GeoObject;
use Presentdv\AddressSuggestions\GeoObject\GeoObjectInterface;

class DadataSuggestionsService extends AddressSuggestionsService
{
    private const QUERY_MAX_LENGTH = 300;

    private const KHABAROVSK_KLADR_ID = '2700000100000';
    private const KHABAROVSK_FIAS_ID = 'a4859da8-9977-4b62-8436-4e1b98c5d13f';
    private const KHABAROVSK_KRAI_FIAS_ID = '7d468b39-1afa-41ec-8c4f-97a8603cb3d4';

    public function findByKhabarovsk(string $address, int $limit): array
    {
        $body = $this->requestBodyForKhabarovsk();

        return $this->getPrompts($address, $limit, $body);
    }

    public function findByKhabarovskRegion(string $address, int $limit): array
    {
        $body = $this->requestBodyForKhabarovskRegion();

        return $this->getPrompts($address, $limit, $body);
    }

    public function findByRussia(string $address, int $limit): array
    {
        return $this->getPrompts($address, $limit, []);
    }

    /**
     * @param string $address
     * @param int $limit
     * @param array $region_request_body
     * @return GeoObjectInterface[]
     * @throws AddressSuggestionsException|\GuzzleHttp\Exception\GuzzleException
     */
    private function getPrompts(string $address, int $limit, array $region_request_body): array
    {
        if (!$this->addressIsSuitableForSearching($address)) {
            return [];
        }

        $http_headers = [
            'Authorization' => 'Token ' . $this->token,
            'Content-Type' => 'application/json'
        ];

        $body = array_merge($this->commonRequestBody($address, $limit), $region_request_body);

        try {

            $response = $this->client->request('POST', $this->url, [
                'headers' => $http_headers,
                'body' => \GuzzleHttp\json_encode($body, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
            ]);

            $code = $response->getStatusCode();

            if ($code !== 200) {

                $error_msg = "Http ошибка при получении подсказки адреса с dadata.ru. Код ошибки: {$code}";

                throw new AddressSuggestionsException($error_msg);
            }

            $content = $response->getBody()->getContents();

            $decoded_response = \GuzzleHttp\json_decode($content, true);

            $results = [];

            foreach ($decoded_response['suggestions'] as $suggestion) {
                $results[] = GeoObject::fromDadataData($suggestion);
            }

            return $results;

        } catch (RequestException $exception) {

            $this->handleRequestException($exception);
        }
    }

    private function addressIsSuitableForSearching(string $address): bool
    {
        return mb_strlen($address) <= self::QUERY_MAX_LENGTH;
    }

    private function commonRequestBody(string $address, int $limit):array
    {
        return [
            'count' => $limit,
            'locations_boost' => [
                ['kladr_id' => self::KHABAROVSK_KLADR_ID]
            ],
            'restrict_value' => true,
            'query' => $address,
        ];
    }

    private function requestBodyForKhabarovsk():array
    {
        return [
            'locations' => [
                ['city_fias_id' => self::KHABAROVSK_FIAS_ID]
            ],
            'from_bound' => ['value' => 'street'],
            'to_bound' => ['value' => 'house']
        ];
    }

    private function requestBodyForKhabarovskRegion(): array
    {
        return [
            'locations' => [
                ['region_fias_id' => self::KHABAROVSK_KRAI_FIAS_ID]
            ],
            'from_bound' => ['value' => 'city'],
            'to_bound' => ['value' => 'house']
        ];
    }
}