<?php

namespace Presentdv\AddressSuggestions\Service;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Presentdv\AddressSuggestions\AddressSuggestionsException;

abstract class AddressSuggestionsService implements AddressSuggestionsServiceInterface
{
    protected const CLIENT_TIMEOUT = 3;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    private $id;

    public function __construct(string $url, string $token, string $id)
    {
        $this->url = $url;
        $this->token = $token;

        $this->client = new Client([
            'timeout' => self::CLIENT_TIMEOUT
        ]);

        $this->id = $id;
    }

    protected function getId(): string
    {
        return $this->id;
    }

    /**
     * @throws AddressSuggestionsException
     */
    protected function handleRequestException(RequestException $exception)
    {
        $error_msg = "Ошибка запроса к {$this->getId()}. Message: {$exception->getMessage()}\nRequest:\n" . Psr7\str($exception->getRequest());

        if ($exception->hasResponse()) {
            $error_msg .= "\nResponse:\n" . Psr7\str($exception->getResponse());
        }

        throw new AddressSuggestionsException($error_msg);
    }
}