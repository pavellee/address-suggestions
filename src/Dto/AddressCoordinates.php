<?php

namespace Presentdv\AddressSuggestions\Dto;


class AddressCoordinates
{
    /** @var float|null */
    private $latitude;

    /** @var float|null */
    private $longitude;

    /** @var bool */
    private $exact;

    public function __construct($latitude, $longitude, bool $exact)
    {
        $this->latitude = ($latitude !== null) ? (float) $latitude : null;
        $this->longitude = ($longitude !== null) ? (float) $longitude : null;
        $this->exact = $exact;
    }

    public function isEmpty():bool
    {
        return empty($this->latitude) || empty($this->longitude);
    }

    public function latitude():? float
    {
        return $this->latitude;
    }

    public function longitude():? float
    {
        return $this->longitude;
    }

    public function exact(): bool
    {
        return $this->exact;
    }
}