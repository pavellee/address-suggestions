<?php

namespace Presentdv\AddressSuggestions\Dto;


class AddressInfo
{
    /** @var string */
    private $region;

    /** @var string */
    private $city;

    /** @var string */
    private $village;

    /** @var string */
    private $street;

    /** @var string */
    private $house;

    /**
     * AddressInfo constructor.
     * @param string $region
     * @param string $city
     * @param string $village
     * @param string $street
     * @param string $house
     */
    public function __construct(string $region, string $city, string $village, string $street, string $house)
    {
        $this->region = $region;
        $this->city = $city;
        $this->village = $village;
        $this->street = $street;
        $this->house = mb_strtolower($house);
    }

    public function region(): string
    {
        return $this->region;
    }

    public function city(): string
    {
        return $this->city;
    }

    public function village(): string
    {
        return $this->village;
    }
    public function street(): string
    {
        return $this->street;
    }

    public function house(): string
    {
        return $this->house;
    }

    public function toArray(): array
    {
        $attributes = ['region', 'city', 'village', 'street', 'house'];
        $result = [];

        foreach ($attributes as $attr) {
            $result[$attr] = $this->$attr();
        }

        return $result;
    }
}