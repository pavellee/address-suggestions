<?php

namespace Presentdv\AddressSuggestions\GeoObject;


class ComparisonResult
{
    private $is_similar = false;
    private $is_equal = false;

    public function __construct(GeoObjectInterface $first_geo_object, GeoObjectInterface $second_geo_object)
    {
        $this->compare($first_geo_object, $second_geo_object);
    }

    public function isEqual(): bool
    {
        return $this->is_equal;
    }

    public function isSimilar(): bool
    {
        return $this->is_similar;
    }

    public function isNotEqual(): bool
    {
        return !($this->is_similar || $this->is_equal);
    }

    private function compare(GeoObjectInterface $first_geo_object, GeoObjectInterface $second_geo_object): void
    {
        $first_info = $first_geo_object->addressInfo();
        $second_info = $second_geo_object->addressInfo();

        $first_region = $first_info->region();
        $second_region = $second_info->region();

        if (!$first_region || !$second_region) {
            return;
        }

        $attributes = ['house', 'street', 'village', 'city', 'region'];
        $attributes_for_strpos = ['village', 'city'];
        $first_empty_index = 0;
        $second_empty_index = 0;
        $is_equal = true;
        $is_similar = true;

        foreach ($attributes as $index => $attr) {
            $first_attr_value = $first_info->$attr();
            $second_attr_value = $second_info->$attr();

            $is_current_equal = ($first_attr_value === $second_attr_value) ||
                (in_array($attr, $attributes_for_strpos) && $first_attr_value !== ''
                    && mb_strpos($second_attr_value,$first_attr_value) !== false);

            $is_equal = $is_equal && $is_current_equal;
            $is_similar = $is_similar && ($is_current_equal || ($second_attr_value === '' && $second_empty_index === $index));

            if ($first_attr_value === '' && $index === $first_empty_index) {
                $first_empty_index++;
            }

            if ($second_attr_value === '' && $index === $second_empty_index) {
                $second_empty_index++;
            }

            if ($first_empty_index > $second_empty_index) {
                return;
            }
        }

        if ($is_equal) {
            $this->is_equal = true;
        }

        if ($is_similar) {
            $this->is_similar = true;
        }
    }
}