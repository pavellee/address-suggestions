<?php

namespace Presentdv\AddressSuggestions\GeoObject;


use Presentdv\AddressSuggestions\Dto\AddressCoordinates;
use Presentdv\AddressSuggestions\Dto\AddressInfo;

class GeoObject implements GeoObjectInterface
{
    /** @var string */
    private $address;
    /** @var AddressCoordinates */
    private $coords;
    /** @var AddressInfo */
    private $info;

    private function __construct(string $address, AddressCoordinates $coordinates, AddressInfo $info)
    {
        $this->address = $address;
        $this->coords = $coordinates;
        $this->info = $info;
    }

    public static function fromDadataData(array $data): self
    {
        $address_info = new AddressInfo(
            (string)$data['data']['region_with_type'],
            (string)$data['data']['city'],
            (string)$data['data']['settlement'],
            ($data['data']['street'] === null) ? '' : $data['data']['street_type_full'] . ' ' . $data['data']['street'],
            (string)$data['data']['house']
        );

        if (is_null(($qc_geo = $data['data']['qc_geo']))) {

            $exact = false;

        } elseif ($qc_geo == 0) {

            $exact = true;

        } elseif ($qc_geo == 2 && $address_info->house() === '' && $address_info->street() !== '') {

            $exact = true;

        } elseif ($qc_geo == 3 && empty($address_info->house() || $address_info->street()) && !empty($address_info->village())) {

            $exact = true;

        } elseif ($qc_geo == 4 && empty($address_info->house() || $address_info->street() || $address_info->village()) && !empty($address_info->city())) {

            $exact = true;

        } else {
            $exact = false;
        }

        return new self($data['value'],
            new AddressCoordinates($data['data']['geo_lat'], $data['data']['geo_lon'], $exact),
            $address_info);
    }

    public static function fromNominatimData(array $data): self
    {
        return new self($data['display_name'],
            new AddressCoordinates($data['lat'], $data['lon'], true),
            new AddressInfo(
                $data['address']['state'] ?? '',
                $data['address']['city'] ?? '',
                $data['address']['village'] ?? '',
                $data['address']['road'] ?? '',
                $data['address']['house_number'] ?? ''
            )
        );
    }

    public function changeCoordsIfNewIsMoreExact(AddressCoordinates $new_coordinates): void
    {
        if (!$this->coords->exact() && $new_coordinates->exact() || $this->coords->isEmpty() && !$new_coordinates->isEmpty()) {
            $this->coords = $new_coordinates;
        }
    }

    public function toArray(): array
    {
        return [
            'address' => $this->address,
            'latitude' => $this->coords->latitude(),
            'longitude' => $this->coords->longitude(),
            'exact' => $this->coords->exact()
        ];
    }

    public function isNeedClarifyCoordinates(): bool
    {
        return !$this->coords->exact();
    }

    public function address(): string
    {
        return $this->address;
    }

    public function coords(): AddressCoordinates
    {
        return $this->coords;
    }

    public function addressInfo(): AddressInfo
    {
        return $this->info;
    }

    public function fullAddress(): string
    {
        $info = $this->info;

        $info_array = $info->toArray();

        return implode(', ', array_filter($info_array));
    }
}