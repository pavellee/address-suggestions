<?php

namespace Presentdv\AddressSuggestions\GeoObject;


use Presentdv\AddressSuggestions\Dto\AddressCoordinates;
use Presentdv\AddressSuggestions\Dto\AddressInfo;

interface GeoObjectInterface
{
    public function changeCoordsIfNewIsMoreExact(AddressCoordinates $new_coordinates): void;

    public function addressInfo(): AddressInfo;

    public function coords(): AddressCoordinates;

    public function isNeedClarifyCoordinates(): bool;

    public function address(): string;

    public function fullAddress(): string;

    public function toArray(): array;
}